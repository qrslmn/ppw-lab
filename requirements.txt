Django==3.1
coverage==4.5.4
gunicorn==19.9.0
pytz==2019.2
sqlparse==0.3.0
whitenoise==4.1.4
